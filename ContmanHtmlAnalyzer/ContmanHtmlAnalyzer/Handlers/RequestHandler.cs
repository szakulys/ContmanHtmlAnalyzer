﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ContmanHtmlAnalyzer
{
    public static class RequestHandler
    {
        public static HtmlDocument GetHtmlDocument(string uri)
        {
            if (!IsValidUri(uri))
                uri = "http://" + uri;
            try
            {
                HtmlDocument htmlDocument = new HtmlDocument();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                htmlDocument.Load(stream);
                return htmlDocument;
            }
            catch
            {
                DialogUtils.ShowErrorMsg(Properties.Resources.ErrorMsg_InvalidUrl);
                return null;
            }
        }

        private static bool IsValidUri(string uri)
        {
            return Uri.IsWellFormedUriString(uri, UriKind.Absolute);
        }
    }
}
