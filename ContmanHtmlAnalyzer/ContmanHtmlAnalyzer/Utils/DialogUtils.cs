﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContmanHtmlAnalyzer
{
    public static class DialogUtils
    {
        public static void ShowErrorMsg(string errorMsg)
        {
            MessageBox.Show(errorMsg, Properties.Resources.DialogTitle_Error, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static void ShowInfoMsg(string infoMsg)
        {
            MessageBox.Show(infoMsg, Properties.Resources.DialogTitle_Info, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
