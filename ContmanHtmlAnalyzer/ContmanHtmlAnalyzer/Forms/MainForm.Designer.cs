﻿namespace ContmanHtmlAnalyzer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbxResults = new System.Windows.Forms.GroupBox();
            this.lsvResults = new System.Windows.Forms.ListView();
            this.KeyWords = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Occurrences = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbxUrl = new System.Windows.Forms.GroupBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.btnDownloadHtmlPage = new System.Windows.Forms.Button();
            this.gbxResults.SuspendLayout();
            this.gbxUrl.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxResults
            // 
            this.gbxResults.Controls.Add(this.lsvResults);
            this.gbxResults.Location = new System.Drawing.Point(12, 70);
            this.gbxResults.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxResults.Name = "gbxResults";
            this.gbxResults.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxResults.Size = new System.Drawing.Size(445, 313);
            this.gbxResults.TabIndex = 7;
            this.gbxResults.TabStop = false;
            this.gbxResults.Text = "Wyniki";
            // 
            // lsvResults
            // 
            this.lsvResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.KeyWords,
            this.Occurrences});
            this.lsvResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsvResults.Location = new System.Drawing.Point(3, 17);
            this.lsvResults.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lsvResults.Name = "lsvResults";
            this.lsvResults.Size = new System.Drawing.Size(439, 294);
            this.lsvResults.TabIndex = 0;
            this.lsvResults.UseCompatibleStateImageBehavior = false;
            this.lsvResults.View = System.Windows.Forms.View.Details;
            // 
            // KeyWords
            // 
            this.KeyWords.Text = "Słowo klucz";
            this.KeyWords.Width = 163;
            // 
            // Occurrences
            // 
            this.Occurrences.Text = "Ilość wystąpień";
            this.Occurrences.Width = 160;
            // 
            // gbxUrl
            // 
            this.gbxUrl.Controls.Add(this.txtUrl);
            this.gbxUrl.Controls.Add(this.btnDownloadHtmlPage);
            this.gbxUrl.Location = new System.Drawing.Point(12, 11);
            this.gbxUrl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxUrl.Name = "gbxUrl";
            this.gbxUrl.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbxUrl.Size = new System.Drawing.Size(445, 54);
            this.gbxUrl.TabIndex = 6;
            this.gbxUrl.TabStop = false;
            this.gbxUrl.Text = "Url";
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(5, 21);
            this.txtUrl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(352, 22);
            this.txtUrl.TabIndex = 1;
            this.txtUrl.TextChanged += new System.EventHandler(this.txtUrl_TextChanged);
            this.txtUrl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUrl_KeyDown);
            // 
            // btnDownloadHtmlPage
            // 
            this.btnDownloadHtmlPage.Enabled = false;
            this.btnDownloadHtmlPage.Location = new System.Drawing.Point(364, 20);
            this.btnDownloadHtmlPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDownloadHtmlPage.Name = "btnDownloadHtmlPage";
            this.btnDownloadHtmlPage.Size = new System.Drawing.Size(75, 23);
            this.btnDownloadHtmlPage.TabIndex = 0;
            this.btnDownloadHtmlPage.Text = "Pobierz";
            this.btnDownloadHtmlPage.UseVisualStyleBackColor = true;
            this.btnDownloadHtmlPage.Click += new System.EventHandler(this.btnDownloadHtmlPage_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 395);
            this.Controls.Add(this.gbxResults);
            this.Controls.Add(this.gbxUrl);
            this.Name = "MainForm";
            this.Text = "Analizer";
            this.gbxResults.ResumeLayout(false);
            this.gbxUrl.ResumeLayout(false);
            this.gbxUrl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxResults;
        private System.Windows.Forms.ListView lsvResults;
        private System.Windows.Forms.ColumnHeader KeyWords;
        private System.Windows.Forms.ColumnHeader Occurrences;
        private System.Windows.Forms.GroupBox gbxUrl;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.Button btnDownloadHtmlPage;
    }
}

