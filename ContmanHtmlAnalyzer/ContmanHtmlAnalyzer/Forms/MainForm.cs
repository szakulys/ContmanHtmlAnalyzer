﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace ContmanHtmlAnalyzer
{
    public partial class MainForm : Form
    {
        #region Pola prywatne

        private string titlePath = "//head/title";
        private string bodyPath = "//body";
        private char[] splits = new char[] { '.', '?', '!', ' ', ';', ':', ',', '\r', '\n', '(', ')', '-', '_', '@', '#', '&', '[', ']', '|', '\\', '/' };

        #endregion Pola prywatne

        #region Konstruktory

        public MainForm()
        {
            InitializeComponent();
        }

        #endregion Konstruktory

        #region Metody prywatne

        private string SetEncoding(string text)
        {
            byte[] bytes = Encoding.Default.GetBytes(text);
            return Encoding.UTF8.GetString(bytes);
        }

        #endregion Metody prywatne

        #region Obsługa zdarzeń

        private void btnDownloadHtmlPage_Click(object sender, EventArgs e)
        {
            HtmlAgilityPack.HtmlDocument htmlDocument = null;
            lsvResults.Items.Clear();

            // Pobranie dokumentu html
            htmlDocument = RequestHandler.GetHtmlDocument(txtUrl.Text);

            if (htmlDocument != null)
            {
                // Utworzenie listy słów kluczowych
                string encodedText = SetEncoding(htmlDocument.DocumentNode.SelectSingleNode(titlePath).InnerText);
                List<string> keyWords = encodedText.ToLower().Split(splits, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList();

                HtmlNode node = htmlDocument.DocumentNode.SelectSingleNode(bodyPath);

                List<string> htmlTextList = new List<string>();
                if (node != null)
                {
                    // Konwersja tekstu body na listę słów
                    encodedText = SetEncoding(node.InnerText);
                    htmlTextList = encodedText.ToString().ToLower().Split(splits, StringSplitOptions.RemoveEmptyEntries).ToList();
                }

                foreach (string word in keyWords)
                {
                    // Obliczenie i wyświetlenie wyników
                    int source = htmlTextList.Count(x => x.Equals(word));
                    lsvResults.Items.Add(new ListViewItem(new string[] { word, source.ToString() }));
                }

                if (lsvResults.Items.Count == 0)
                    DialogUtils.ShowInfoMsg(Properties.Resources.InfoMsg_NoResults);
            }
        }
        
        private void txtUrl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && txtUrl.TextLength > 0)
            {
                e.SuppressKeyPress = true;
                btnDownloadHtmlPage.PerformClick();
            }
        }

        private void txtUrl_TextChanged(object sender, EventArgs e)
        {
            if (txtUrl.TextLength > 0)
                btnDownloadHtmlPage.Enabled = true;
            else
                btnDownloadHtmlPage.Enabled = false;
        }

        #endregion Obsługa zdarzeń
    }
}
