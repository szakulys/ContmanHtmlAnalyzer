﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ContmanHtmlAnalyzer;
using HtmlAgilityPack;

namespace ContmanHtmlAnalyzerTests
{
    [TestClass]
    public class RequestHandlerTest
    {
        [TestMethod]
        public void TestGetHtmlDocument()
        {
            HtmlDocument actualDocument = RequestHandler.GetHtmlDocument("contman.pl");

            Assert.IsNotNull(actualDocument);
        }
    }
}
